package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;

    public Pokemon(String name){
    this.random = new Random();
    this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
    this.maxHealthPoints = this.healthPoints;
    this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    this.name = name;

    }
    
    public String getName(){
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        return (getCurrentHP() > 0);

    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);

        System.out.println(this.name + " attacks " + target);

        
    }

    @Override
    public void damage(int damageTaken) {
        damageTaken = Math.max(damageTaken, 0);
        damageTaken = Math.min(damageTaken, getCurrentHP());
        this.healthPoints -= damageTaken;
        
        System.out.println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + "/" + this.maxHealthPoints + " HP"); 
    }
    
    
    @Override
    public String toString() {
        return (name + " HP: (" + this.healthPoints + "/" + this.maxHealthPoints + ") STR: " + this.strength);
    }

}